import sys
from tissuelab.image import *
from library_newImsave import *

# Segment nuclei using membrane segmentation
#
membraneFileName = "/home/Niklas/jose/SUN1_pl23/T4/C2-crop_310715_Plant23T3.tif"
nFName = "/home/Niklas/jose/SUN1_pl23/T4/C1-crop_310715_Plant23T3.tif"
hmin_membrane = 2
asf_membrane = 2
sigma_membrane = 2

hmin_nucleus = 2 # bugged. copies from membrane.
asf_nucleus = 2 # bugged. copies from membrane.
sigma_nucleus = 2 # bugged. copies from membrane.

segmentTwoChannel(membraneFileName, nFName, hmin_membrane, asf_membrane, sigma_membrane, hmin_nucleus, asf_nucleus, sigma_nucleus)
#
# # Clean segmented image from large cells
#
# from tissuelab.image import removeLargeCells
# segImage = "/home/Niklas/jose/SUN1_pl23/T1/C1-crop_290715_Plant23T0_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00.tif"
# maxVoxelNb = 60000
# removeLargeCells(segImage, maxVoxelNb, 1)

# #  MARS segmentation using membrane channel

imageFName = "/home/Niklas/jose/SUN1_pl23/T1/C2m-crop_290715_Plant23T0.tif"
hmin = 2
asf = 2
sigma = 2
resolution = (0.1465059, 0.1465059, 0.1502038)
segment(imageFName, hmin, asf, sigma, resolution)

# Clean image
# segImageName = "/home/Niklas/jose/SUN1_pl23/T1/C2m-crop_290715_Plant23T0_hmin_2_asf_2.00_s_2.00.tif"
# radius = 2
# clean(segImageName, radius)


# # Extract L1 nuclei shiftLabels

from tissueviewer.image import *

NIm = "/home/Niklas/jose/CRWN4_pl11/T4/T4_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00_bigCellsRemoved.tif"
SIm = "/home/Niklas/jose/CRWN4_pl11/T4/T4_M_hmin_2_asf_2.00_s_2.00.tif"
print "cellsToShow=" + str(list(extractNL1UsingMemSegmentation(NIm, SIm)))


##################################################################################


# ##  Extract L1 cells and match with nuclei labels

# from library_newImsave import segment

# mSegmentedImageFName = "/home/jose/Projects/ATML1_allData/membraneSegmentation/C2-#5_t_16.tif"
# resolution=(0.2965236,0.2965236,0.8)
# segment(mSegmentedImageFName, 2, 3, 3, resolution)

# ###  Extract L1 cells and match with nuclei labels

# from tissuelab.image import extractL1L2
# mSegmentedImageFName = "/home/jose/Projects/KF nuclear shape/150817-SUN1-timecourse/segmentations/t0_M_hmin_2_asf_2.00_s_2.00_clean_5.tif"
# nSegmentedImageFName= "/home/jose/Projects/KF nuclear shape/150817-SUN1-timecourse/segmentations/t0_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00.tif"

# im, tags = tiffread(mSegmentedImageFName)
# nim, ntags = tiffread(nSegmentedImageFName)

# L1, L2 = extractL1L2(im,1)

# # for z in xrange(im.shape[2]):
# #     for i in xrange(im.shape[0]):
# #        for j in xrange(im.shape[1]):
# #           im[z, i, j]


###

# from tissuelab.image import shiftLabels

# segmentedImageFileName = "/home/jose/Desktop/ATML1_pipeline_test/translational8_timecourse/Segmentation/t8_MARS.tif"
# targetFileName = "/home/jose/Desktop/ATML1_pipeline_test/translational8_timecourse/Segmentation/t8_MARS_to_mgx.tif"

# costanza_to_mars=2
# costanza_to_mgx=1
# mars_to_costanza=-2
# mars_to_mgx=-1
# mgx_to_costanza=-1
# mgx_to_mars=1

# shiftLabels(segmentedImageFileName, targetFileName, mars_to_mgx)
