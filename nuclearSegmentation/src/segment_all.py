import os
import numpy as np
import library_newImsave as lib
import tissueviewer.image
import tissueviewer.tvtiff

try:
    image_directory = os.path.abspath(os.path.join(
        os.path.realpath(__file__),
        os.pardir,
        "images"
    ))
except(NameError):
    image_directory = os.path.abspath(os.path.join(
        os.path.curdir,
        os.pardir,
        "images"
    ))
print("Image directory used: " + image_directory)

# The script assumes a directory structure of:
# NuclearShape/ # the root directory of this project
#    images/
#        plant/ #e.g. SUN1_p18
#           images/
#               t1/
#                  image files, preprocessed in ImageJ.
#               t2/
#               t3/
#               t3/

image_parameters = {
    "SUN1_p18": {
        "T1": {
            "membrane_file": "T1_M.tif",
            "nuclear_file": "T1_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold":
            30000,  # remove "cells" which have more voxels than this.
        },
        "T2": {
            "membrane_file": "T2_M.tif",
            "nuclear_file": "T2_N.tif",
            "hmin_membrane": 3,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
        "T3": {
            "membrane_file": "T3_M.tif",
            "nuclear_file": "T3_N.tif",
            "hmin_membrane": 3,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
        "T4": {
            "membrane_file": "T4_M.tif",
            "nuclear_file": "T4_N.tif",
            "hmin_membrane": 3,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
    },
    "SUN1_p23": {
        "T1": {
            "membrane_file": "C2m-crop_290715_Plant23T0.tif",
            "nuclear_file": "C1-crop_290715_Plant23T0.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 40000,
        },
        "T2": {
            "membrane_file": "C2-crop_300715_Plant23T1.tif",
            "nuclear_file": "C1-crop_300715_Plant23T1.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 40000,
        },
        "T3": {
            "membrane_file": "C2-crop_300715_Plant23T2.tif",
            "nuclear_file": "C1-crop_300715_Plant23T2.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 40000,
        },
        "T4": {
            "membrane_file": "C2-crop_310715_Plant23T3.tif",
            "nuclear_file": "C1-crop_310715_Plant23T3.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 40000,
        },
    },
    "SUN1_p6": {
        "T1": {
            "membrane_file": "T1_M.tif",
            "nuclear_file": "T1_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 25000,
        },
        "T2": {
            "membrane_file": "T2_M.tif",
            "nuclear_file": "T2_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 2,
            "sigma_nucleus": 2,
            "max_threshold": 25000,
        },
        "T3": {
            "membrane_file": "T3_M.tif",
            "nuclear_file": "T3_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 25000,
        },
        "T4": {
            "membrane_file": "T4_M.tif",
            "nuclear_file": "T4_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
    },
    "SUN1_p9": {
        "T1": {
            "membrane_file": "T1_M.tif",
            "nuclear_file": "T1_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
        "T2": {
            "membrane_file": "T2_M.tif",
            "nuclear_file": "T2_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
        "T3": {
            "membrane_file": "T3_M.tif",
            "nuclear_file": "T3_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        },
        "T4": {
            "membrane_file": "T4_M.tif",
            "nuclear_file": "T4_N.tif",
            "hmin_membrane": 2,
            "hmin_nucleus": 2,
            "asf_membrane": 2,
            "asf_nucleus": 2,
            "sigma_membrane": 3,
            "sigma_nucleus": 2,
            "max_threshold": 30000,
        }
    }
}


for plant in image_parameters.keys():
    print(plant)
    for (time, parameters) in sorted(image_parameters[plant].items()):
        m_file = os.path.join(
            image_directory,
            plant,
            "images",
            time.lower(),
            parameters["membrane_file"]
        )
        n_file = os.path.join(
            image_directory,
            plant,
            "images",
            time.lower(),
            parameters["nuclear_file"]
        )
        if not os.path.isfile(m_file):
            raise m_file + " does not exit!"
        if not os.path.isfile(n_file):
            raise n_file + " does not exit!"
        #
        print("segmenting " + plant + " for " + time)
        resolution = tissueviewer.tvtiff.getTiffFileResolution(m_file)
        print(resolution)
        #
        membrane_seg_file = lib.segment(
            m_file,
            parameters["hmin_membrane"],
            parameters["asf_membrane"],
            parameters["sigma_membrane"],
            resolution,
        )
        nuclear_seg_file = lib.segmentTwoChannel(
            m_file,
            n_file,
            parameters["hmin_membrane"],
            parameters["asf_membrane"],
            parameters["sigma_membrane"],
            parameters["hmin_nucleus"],
            parameters["asf_nucleus"],
            parameters["sigma_nucleus"],
            resolution,
        )
        #
        print(nuclear_seg_file)
        nuclear_seg_file = lib.removeLargeCells(
            nuclear_seg_file,
            parameters["max_threshold"]
        )
        image_parameters[plant][time]["nuclear_segmented_file"] = nuclear_seg_file
        image_parameters[plant][time]["membrane_segmented_file"] = membrane_seg_file
        #
        # Get the labels for the nuclei in the L1 (epidermis).
        nuclear_l1 = tissueviewer.image.extractNL1UsingMemSegmentation(
            nuclear_seg_file,
            membrane_seg_file
        )
        nuclear_l1_folder = os.path.join(
            image_directory,
            plant,
            "nuclear_l1_labels"
        )
        nuclear_l1_file_path = os.path.join(
            nuclear_l1_folder,
            "L1_labels_" + nuclear_seg_file.split("/")[-1][:-4] + ".csv"
        )
        if not os.path.exists(nuclear_l1_folder):
            os.makedirs(nuclear_l1_folder)
        np.savetxt(nuclear_l1_file_path, nuclear_l1, fmt="%i", delimiter=",\n")


