############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@slcu.cam.ac.uk>
#
############################################################################


from openalea.image.spatial_image import SpatialImage
from scipy.ndimage.filters import gaussian_filter
from openalea.image.all import imread, imsave
import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage
import scipy.ndimage as nd



def applyPalette(im, base_dict, threshold=None, dec=10000):
    outliers=set(np.array(base_dict.keys())[np.array(base_dict.values())>threshold])
    palette=np.zeros(np.max(im)+1, dtype=np.uint16)
    for i in outliers:
        palette[i]=base_dict[i]*dec
    return palette[im] 

def consecutiveLabels(im):
    if (set(np.unique(im))!=set(range(1, np.max(im)+1))):
        im[im==0]=im.max()+1
        histo=nd.histogram(im, min=0, max=np.max(im), bins=np.max(im)+1)
        if np.argsort(histo)[-1]!=1:
            im[im==1]=im.max()+1
            im[im==np.argsort(histo)[-1]]=1
        labels=np.unique(im)
        labels.sort()
        labels_con=np.linspace(1, len(labels), len(labels)).astype(np.uint16)
        mapping=dict(zip(labels, labels_con))
        return applyPalette(im, mapping, dec=1), zip(labels, labels_con)
    else:
        return im, None

if __name__ == "__main__":
    imageName = "/media/YassinSLCU/storage/data/Yassin/4DAtlas/segmentations/All/t81/YR_1_t81_segmented.tif"    
    newImage = imread(imageName)
    newImage, d = consecutiveLabels(newImage)
    print d
    imsave(imageName[:-4] + "_labelsOK.tif", SpatialImage(newImage))
    
    
    