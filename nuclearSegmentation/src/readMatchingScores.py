import cPickle

# fName = "/home/Niklas/jose/SUN1_pl23/registration/24h_to_36h_ALT5/matchingScores_24h_to_36h_ALT5.pkl"

# fName = "/home/Niklas/home3/projects/JoseNuclearShape/images/CRWN4_pl11/registration/3h_to_4h_ALT3/matchingScores_3h_to_4h_ALT3.pkl"

def read_matching_scores(fName):
    fobj = file(fName)
    matching, scores = cPickle.load(fobj)
    fobj.close()


    mothersDaughters = {}
    for m in matching:
        mothersDaughters.setdefault(m[1],[]).append(m[0])


    print mothersDaughters

    fobj = file(fName[:-4] + ".txt", "wb")
    for m, dList in mothersDaughters.iteritems():
        line = str(m) + str(" : ") + str(dList) + "\n"
        fobj.write(line)

    fobj.close()

if __name__ == "__main__":
    fName = "/home/Niklas/home3/projects/JoseNuclearShape/images/SUN1_p6/tracking/2h_to_3h_ALT3/matchingScores_2h_to_3h_ALT3.pkl"
    print(fName)
    read_matching_scores(fName)
