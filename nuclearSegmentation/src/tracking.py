import os
import sys
import numpy as np
from openalea.image.serial.basics import imread, imsave
from vplants.mars_alt.alt.candidate_lineaging import candidate_lineaging
# from openalea.image.all import SpatialImage, SpatialImageAnalysis
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import SpatialImage
from vplants.mars_alt.alt.optimal_lineage import optimal_lineage
from scipy import ndimage as nd
import cPickle
from makeLabelsConsecutive import consecutiveLabels
from shutil import copy2
import readMatchingScores

from tissuelab.tltiff import getTiffFileResolution
## Load from parameter config file
from parameters import pathApplyTrsf, pathBlockmatching, t0Path, t1Path, seg0Path, seg1Path, savePath, matchingNamePostfix, ALTParameter
import time

startTime = time.time()

elastic_sigma = ALTParameter
fluid_sigma = ALTParameter
t0Resolution = getTiffFileResolution(t0Path)
t1Resolution = getTiffFileResolution(t1Path)
seg0Resolution = t0Resolution
seg1Resolution = t1Resolution

t0=imread(t0Path)
t1=imread(t1Path)
t0.resolution= t0Resolution
t1.resolution= t1Resolution
t0seg = imread(seg0Path)
t1seg = imread(seg1Path)
t0seg.resolution = seg0Resolution
t1seg.resolution = seg1Resolution

runFromBegining = True

if runFromBegining:
    imsave('%st0.inr'%savePath, t0)
    imsave('%st1.inr'%savePath, t1)
    os.system(pathBlockmatching +
              " -ref %st1.inr"%savePath +
              " -flo %st0.inr"%savePath +
              " -res %st0_affine_registered.inr"%savePath +
              " -res-trsf %saffine_full_trsf"%savePath +
              " -trsf-type affine" +
              " -estimator wlts" +
              " -pyramid-highest-level 5" +
              " -pyramid-lowest-level 2" +
              " -lts-fraction 0.55")



    os.system(pathBlockmatching +
              " -ref %st1.inr"%savePath +
              " -flo %st0.inr"%savePath +
              " -init-trsf %saffine_full_trsf"%savePath +
              " -res %st0_NL_registered.inr"%savePath +
              " -res-trsf %sNL_trsf.inr"%savePath +
              " -trsf-type vectorfield" +
              " -estimator wlts" +
              " -py-gf" +
              " -pyramid-highest-level 5" +
              " -pyramid-lowest-level 2" +
              " -elastic-sigma %f %f %f"%(elastic_sigma, elastic_sigma, elastic_sigma) +
              " -fluid-sigma %f %f %f"%(fluid_sigma, fluid_sigma, fluid_sigma))

t0seg.resolution=t0.resolution
imsave('%st0Seg.inr'%savePath, t0seg)

os.system(pathApplyTrsf +
          " %st0Seg.inr"%savePath +
          " %sseg_t0_on_t1.inr"%savePath +
          " -trsf %sNL_trsf.inr"%savePath +
          " -nearest") # should use linear for intensity images



seg_t0_on_t1 = imread("%sseg_t0_on_t1.inr"%savePath)
t1_seg = imread(seg1Path)
seg_t0_on_t1[seg_t0_on_t1 == 0 ] = 1

im0, dict0 =  consecutiveLabels(seg_t0_on_t1)
seg_t0_on_t1 = SpatialImage(im0) # we need labels from 1 to n with no gap between labels and 1 as the background

im1, dict1 = consecutiveLabels(t1_seg)
t1_seg = SpatialImage(im1)


seg_t0_on_t1.resolution = t1_seg.resolution = t1.resolution

imsave("%st0_on_t1_seg_homogenise.tif"%savePath, seg_t0_on_t1)
imsave("%st1_seg_homogenise.tif"%savePath, t1_seg)

fobj = file("%sconsecutiveLabelsDicts.pkl"%savePath, "w")
cPickle.dump((dict0, dict1), fobj)
fobj.close()


t0MaxLabel = seg_t0_on_t1.max()
t1MaxLabel = t1_seg.max()


an0 = SpatialImageAnalysis(seg_t0_on_t1)
an1 = SpatialImageAnalysis(t1_seg)

volumes1 = dict(zip(an1.labels(), an1.volume(real = False)))
boundingboxes0 = an0.boundingbox()
labels_bb0 = dict(zip(an0.labels(), boundingboxes0))
matrix = np.zeros((t0MaxLabel + 1, t1MaxLabel + 1), dtype=np.float32)

sizeHere = len(labels_bb0)
counterHere = 0
for cell, bb in labels_bb0.iteritems():
    print counterHere / float(sizeHere)
    counterHere += 1
    image_rec = t1_seg[bb][seg_t0_on_t1[bb] == cell]
    vol = np.sum(np.ones_like(image_rec))
    labels = np.unique(image_rec)
    recouv = dict()
    for lab in labels:
        if lab!=1:
            recouv[lab] = 2 * nd.sum(image_rec == lab)/ np.float32( vol + volumes1[lab])
        else :
            recouv[lab] = nd.sum(image_rec == lab) / np.float32(vol)
    for cell_recouv in recouv:
        matrix[cell, cell_recouv]=recouv[cell_recouv]

# TO DOUBLE CHECK


#matching=zip(range(2, t1MaxLabel), np.argmax(matrix, axis=1)+2) # chaque fille -> une mere

if dict0 != None:
    t0Ont1ConcatTot0Dict = dict((j, i) for i, j in dict0)
else:
    t0Ont1ConcatTot0Dict = dict((i, i) for i in xrange(np.max(seg_t0_on_t1) + 1))

if dict1 != None:
    t1ConcatTot1Dict = dict((j, i) for i, j in dict1)
else:
    t1ConcatTot1Dict = dict((i, i) for i in xrange(np.max(t1_seg) + 1))





matching = []
scoresList = []
for d in xrange(2, t1MaxLabel + 1):
    score=np.max(matrix[:, d])
    matching.append((t1ConcatTot1Dict[d], t0Ont1ConcatTot0Dict[np.argmax(matrix[:, d])] ))
    scoresList.append(score)

fobj = file("%smatchingScores_%s.pkl"%(savePath, matchingNamePostfix), "w")
cPickle.dump((matching, scoresList), fobj)
fobj.close()

print "Elapsed time = ", time.time() - startTime

readMatchingScores.read_matching_scores("%smatchingScores_%s.pkl"%(savePath, matchingNamePostfix))
copy2("parameters.py", savePath + "parameters_used.py")

# matching = []
# scoresList = []
# for d in xrange(2, t1MaxLabel + 1):
#     score=np.max(matrix[:, d])
#     matching.append((d, np.argmax(matrix[:, d])))
#     scoresList.append(score)
#
# fobj = file("matchingScoresList_%s.pkl"%matchingNamePostfix, "w")
# pickle.dump((matching, scoresList), fobj)
# fobj.close()
