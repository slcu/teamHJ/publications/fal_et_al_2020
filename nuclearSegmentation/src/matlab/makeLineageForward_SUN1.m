%%
% ---------------------------------------------------------------------
% Make lineage for an initial cell number
% Forward tracking: select cell at first time point to trace lineage
% forward
% Input:
% data_tracking - lineage table
% data_costanza - costanza data table
% startCell - segmentation ID of select cell at first time point
% Output:
% lineageTrace - vector/matrix of lineage IDs throughout time course
% ---------------------------------------------------------------------

function [lineageTrace] = makeLineageForward_SUN1(data_tracking,data_costanza,startCell)

    lineageTrace=[];

    lineageTrace(1)=startCell;

    for i=1:size(data_costanza,2)-1
    
        sizeBefore=size(lineageTrace,1);
    
        for j=1:sizeBefore
        
            if lineageTrace(j,i)==1
            
                lineageTrace(j,i+1)=1;
                
            elseif lineageTrace(j,i)==-1
                
                lineageTrace(j,i+1)=-1;
             
            else
            
                temp=find(data_tracking{i}(:,1)==lineageTrace(j,i));
            
            end
    
            if isempty(temp)==1 
        
                if lineageTrace(j,i)==-1
                
                    lineageTrace(j,i+1)=-1;
                        
                else
                    
                    lineageTrace(j,i+1)=1;
                    
                end
                
                temp=[]; 
                
            elseif isempty(temp)==1 && lineageTrace(j,i)==-1
        
                lineageTrace(j,i+1)=-1;
            
                temp=[]; 
        
            elseif size(temp,1)==1
            
                lineageTrace(j,i+1)=data_tracking{i}(temp,2);
            
                temp=[];
                
            elseif size(temp,1)==2
            
                lineageTrace(j,i+1)=data_tracking{i}(temp(1),2);
                
                lineageTrace(end+1,1:i)=lineageTrace(j,1:i);
          
                lineageTrace(end,i+1)=data_tracking{i}(temp(2),2);
            
                temp=[];
        
            else
        
                lineageTrace(j,i+1)=-1;
            
                temp=[];
            
            end
    
        end
    
    end

end