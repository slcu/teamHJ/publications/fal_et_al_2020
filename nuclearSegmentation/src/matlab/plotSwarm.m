function fig = plotSwarm(data_matrix, highlighted_lineages, y_label, plot_title)

%Define classes, e.g. which rows correspond to a given reporter
t0h=data_matrix(:,1);
t12h=data_matrix(:,2);
t24h=data_matrix(:,3);
t32h=data_matrix(:,4);

%Make cell array with all classes
data={t0h,t12h,t24h,t32h};
fig = figure();
hold;
a = plotSpread(data);

title(plot_title);
ylabel(y_label);%Change as suitable
xlabel('timepoint');
xlim([0.5 4.5]);%Change according to number of classes
%ylim([0 1000])
set(gca,'XTickLabel',{'0h','12h','24h','36h'},'LineWidth',2.5,'FontSize',16);

if ~isempty(highlighted_lineages)
    plot(1:4, data_matrix(highlighted_lineages, :)','Marker','o')
end
% NOTE: shape, size and color of the markers in the plot were changed
% manually by using the Figure Properties menu in the corresponding figure window
end


