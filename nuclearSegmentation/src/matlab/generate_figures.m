%% Section 1
% import variables.
% addpath("/home/Niklas/home3/projects/JoseNuclearShape/images/SUN1_p9/")
addpath("/home/Niklas/home3/projects/JoseNuclearShape/images/SUN1_p18/")
% addpath("/home/Niklas/home3/projects/JoseNuclearShape/images/SUN1_p6/")
% addpath("/home/Niklas/home3/projects/JoseNuclearShape/images/CRWN4_p11/")
addpath("plotSpread/")
addpath("DrosteEffect-Colormaps-from-MatPlotLib2.0-4d7e2bf/")
matlab_parameters;

% Test that L1 labels from python script largely match labels from
% Morphographix.
for i = 1:size(L1_nuclei,2)
    len_py = length(L1_nuclei{i});
    len_mgx = length(label_correspondences{i}(:,2));
    len_intersect = length(intersect(L1_nuclei{i}, label_correspondences{i}(:,2)));
    disp("T" + i + " " + len_intersect/len_mgx + "% of py in mgx; " + len_intersect/len_py + "% of mgx in py")
    if len_intersect/len_mgx < 0.6
        warning("The L1 label file and the label correspondence file should both contain the L1 labels. They do not agree!")
    end
end

%% Section 2 - Get the image resolutions for the time course
resolutions=[];

for k=1:size(filelist_segmented,1)
    
    fname=filelist_segmented(k).name;
    a=strcat(filelist_segmented(k).folder,"/",fname);
    info= imfinfo(a); 
    resolutions(k,1)=info(1).Width;
    resolutions(k,2)=info(1).Height;
    info=[];
    
end
disp("Done with section 2.")

%% Section 3
% Use matlabs "regionprops" to extract features from the segmented data. This takes a few minutes.
nuclearData={};
cell_labels={};
tic()
for k=1:size(filelist_segmented,1)
       
    fname=filelist_segmented(k).name;%segmented filename
    
    % Get number of slices in both raw and segmented stacks
    a=strcat(filelist_segmented(k).folder,"/",fname);%full path
    disp("Processing file " + a)
    info = imfinfo(a); 
    num_images = numel(info);%nr of slices in segmented stack
    
    cell_labels{k} = [];
    allFrames = cell(1,num_images);
    nr_cells = zeros(1,num_images);
    % Get region properties for all slices of the segmented stack
    for i=1:num_images % loops through each segmented slice
        
        disp("image " + k + ", i=" + i + " of " + num_images)
        img= imread(a,i);
        %for each slice get the properties of each segmented region
        %(corresponding to a nucleus)
        % Each element on 'allFrames' cell array contains the list of
        % properties for each segmented nucleus in that slice
        allFrames{1,i}=regionprops(img,'Area','BoundingBox','Centroid','ConvexHull','Eccentricity','MajorAxisLength','MinorAxisLength','Orientation','PixelList');
        nr_cells(1,i)=size(allFrames{1,i},1); %nr of nuclei segmented in each slice
        labels_in_slice = unique(img);
        cell_labels{k} = union(cell_labels{k}, labels_in_slice);
    end
    
    % Check that all the labels from morphographx also exists in the
    % segmented image.
    if length(union(cell_labels{k}, label_correspondences{k}(:,2))) ~= length(cell_labels{k})
        display(setdiff(union(cell_labels{k}, label_correspondences{k}(:,2)), cell_labels{k}))
        error("The Mgx correspondence uses a label which does not exist in the segmentation.")
    end
    
    total_cells(k)=max(nr_cells(1,:)); %total number of segmented nuclei over all slices
%     total_cells(k)=length(cell_labels{k}); %total number of segmented nuclei over all slices

    % Loop through all slices for each nucleus, select largest slice and
    % calculate shape parameters
    for m=1%:size(allFrames{1,i},1) 
        
        nr=[1:1:total_cells(k)];

        for j=1:size(nr,2)
            
            count_j(j)=j;
            
            allAreas=[];
    
            for i=1:size(allFrames(m,:),2)
                
                count_i(j,i)=i;
                
                if size(allFrames{m,i},1)>=nr(j)

                    allAreas(i)=allFrames{m,i}(nr(j)).Area;
                       
                else
                    allAreas(i)=0;
        
                end

            end
            
            %allAreas_count(j,:)=allAreas;
            [maxVal,index]=max(allAreas);%maximum area annotation 
            
            if(max(allAreas)==0)
                
                % Shape parameter annotation
                nuclearData{k,j}.id=nr(j);                
                nuclearData{k,j}.PixelList=[0,0,0];
                nuclearData{k,j}.Area=0;
                nuclearData{k,j}.Eccentricity=0;
                nuclearData{k,j}.AspectRatio=0;
                nuclearData{k,j}.Area_corrected=0;
                
                
            else
                % Shape parameter annotation
                nuclearData{k,j}=allFrames{m,index}(nr(j));
                nuclearData{k,j}.id=nr(j);
                nuclearData{k,j}.AspectRatio=nuclearData{k,j}.MajorAxisLength / nuclearData{k,j}.MinorAxisLength;
                nuclearData{k,j}.PixelList(:,3)=index;
                nuclearData{k,j}.AreaEccentricity=nuclearData{k,j}.Area*nuclearData{k,j}.Eccentricity;
                nuclearData{k,j}.Area_corrected=nuclearData{k,j}.Area*pixelSizes(1,k)*pixelSizes(2,k);
                
            end          
        end
    end
end
clear allFrames nr_cells

disp("Done with section 3")
toc()

%% SECTION 4

%Collect all quantified nuclear variables into individual arrays

% Slice area (NOT CORRECTED FOR PIXEL SIZES)
% In practice corresponds to the number of pixels in a given slice
    for i=1:size(nuclearData,1)
    
    for j=2:size(nuclearData,2)
        
        if isempty(nuclearData{i,j})==0
            
            allArea(i,j)=nuclearData{i,j}.Area;
            
        end
        
    end

end
allArea(allArea==0)=NaN;%replace zeros with NaN (facilitates statistic calculations)

% Slice area (CORRECTED FOR PIXEL SIZES)
% In practice corresponds to the number of pixels in a given slice times
% the size (X and Y) of each pixel
for i=1:size(nuclearData,1)
    for j=2:size(nuclearData,2)
        if isempty(nuclearData{i,j})==0
            
            allArea_corrected(i,j)=nuclearData{i,j}.Area * pixelSizes(1,i)*pixelSizes(2,i);
            
        end
        
    end

end
allArea_corrected(allArea_corrected==0)=NaN;%replace zeros with NaN (facilitates statistic calculations)

% Slice aspect ratio
for i=1:size(nuclearData,1)
    
    for j=2:size(nuclearData,2)
        
        if isempty(nuclearData{i,j})==0
            
            allAspectRatio(i,j)=nuclearData{i,j}.AspectRatio;
            
        end
        
    end

end
allAspectRatio(allAspectRatio==0)=NaN;%replace zeros with NaN (facilitates statistic calculations)

% Slice eccentricity
for i=1:size(nuclearData,1)
    
    for j=2:size(nuclearData,2)
        
        if isempty(nuclearData{i,j})==0
            
            allEccentricity(i,j)=nuclearData{i,j}.Eccentricity;
            
        end
        
    end

end
allEccentricity(allEccentricity==0)=NaN;%replace zeros with NaN (facilitates statistic calculations)
disp("Done with section 4") 

%% SECTION 5

% TRACKING: Make 'forward' cell lineages starting at the first point of the time course
% Connects cell IDs from Costanza imported files throughout different time
% points using MorphoGraphX-generated correspondences between two
% consecutive time points
% 
% Requires functions 'makeLineageForward' and 'make2Dtimecourse'
nuclei_list={};
for i = 1:size(nuclearData, 1)
    nuclei_list{i} = [2:1:sum(~cellfun(@isempty,nuclearData(i,:)),2)+1]';
end

for i=1:size(nuclei_list{1},1)%loop through all nuclei present at time 0 (starting point)
    
    %Each element of 'lineageTrace' cell array contains a mXn vector of ids
    %where the m rows correspond to the number of total lineages originated
    %from the inicial cell (larger than 1 if divisions occur) and the n
    %columns correspond to the time points.
    %
    %NOTE:when divisions occur, two consecutive rows will have repeated
    %elements, corresponding to the IDs of the common ancestor.
    % Also, id=1 in this context means the cell could not be found in a given
    % timpoint, i.e., id=1 marks the point at which that the lineage was lost
    lineageTrace{i} = makeLineageForward_SUN1(data_tracking,nuclei_list,nuclei_list{1}(i,1));
    
end

% Using the ids of each lineage obtained above, produce an array that
% compiles all the lineages ('allLineageTrace') and the corresponding
% individual arrays for the different variables. All arrays have the same
% dimensions so that variable values can be mapped to the IDs in
% 'allLineageTrace'
[lineage_area,lineage_eccentricity,lineage_aspect_ratio,lineage_area_corrected] = make2DTimecourse_SUN1(lineageTrace{1},allArea,allEccentricity,allAspectRatio,allArea_corrected)

allLineageTrace=lineageTrace{1};

for i=2:size(lineageTrace,2) 
    
    allLineageTrace=cat(1,allLineageTrace,lineageTrace{i});
    
end

for i=2:size(lineageTrace,2)
    
%     i %counter (progress on number of lineages)
    
    [temp_lineage_area,temp_lineage_eccentricity,temp_lineage_aspect_ratio,temp_lineage_area_corrected] = make2DTimecourse_SUN1(lineageTrace{i},allArea,allEccentricity,allAspectRatio,allArea_corrected);
    
    lineage_area=cat(1,lineage_area,temp_lineage_area);
    lineage_eccentricity=cat(1,lineage_eccentricity,temp_lineage_eccentricity);
    lineage_aspect_ratio=cat(1,lineage_aspect_ratio,temp_lineage_aspect_ratio);
    lineage_area_corrected=cat(1,lineage_area_corrected,temp_lineage_area_corrected);
  
end
disp("Done with section 5")

%% SECTION 6
% MAKE CURVATURE TABLES USING LABEL CORRESPONDENCES




cell_curvatures=zeros(size(nuclearData,1),size(nuclearData,2));
count=zeros(4,1);
count_duplicates=zeros(4,1);

for i=1:size(nuclearData,1)
    for j=1:size(nuclearData,2)
        if ismember(j,label_correspondences{i}(:,2))==1
            
            a=find(label_correspondences{i}(:,2) == j);
            
            if size(a,1)==1

                correspond=label_correspondences{i}(a,1);
                b=find(curvatures{i}(:,1) == correspond);
            
                
                %%% Bugfix: ignore MGX cells without mapping to
                %%% segmentation
                if isscalar(b)
                    cell_curvatures(i,j)=curvatures{i}(b,2);
                    count(i,1)=count(i,1)+1;
                else
                    i
                    j
                    b
                    warning("I'm uneasy about the script getting here.  ")
                end
            elseif size(a,1)>1
                count_duplicates(i,1)=count_duplicates(i,1)+1;
                
            end
            
            a=[];
            
        end
        
    end
    
end
disp("Done with section 6")

%% Section 7 - FIND BOUNDARY BY FITTING CURVE ON NEGATIVE CURVATURE NUCLEI

%Get centroids of nuclei with negative curvature
boundary_nuclei={};
k=1;
for i=1:size(cell_curvatures,1)
    for j=1:size(cell_curvatures,2)
        
        if cell_curvatures(i,j)<0 % && isfield(nuclearData{4, j}, 'Centroid')
            
            boundary_nuclei{i}(k)=j;
            k=k+1;
            
        end
        
    end
    
    k=1;
    
end
        
L1_nuclei_boundary_selected_T4=boundary_nuclei{4}; %change accordingly (negative curvatures)

xy_coordinates_boundary_T4=[];
for i=1:size(L1_nuclei_boundary_selected_T4,2)
    i;
    xy_coordinates_boundary_T4(i,1:2)=nuclearData{4,L1_nuclei_boundary_selected_T4(i)}.Centroid;
    
end

xy_coordinates_rest_T4=[];

for i=1:size(L1_nuclei{4},1) %Change time point accordingly
    
    if ismember(L1_nuclei{4}(i),L1_nuclei_boundary_selected_T4)==0
    
        xy_coordinates_rest_T4(i,1:2)=nuclearData{4,L1_nuclei{4}(i)}.Centroid;
        
    end
    
end

%Fit curve using boundary centroids

% p = fit(xy_coordinates_boundary_T4(:,1),xy_coordinates_boundary_T4(:,2),'poly1','Exclude', xy_coordinates_boundary_T4(:,2)<200,'Exclude', xy_coordinates_boundary_T4(:,1)>400);
% mask = xy_coordinates_boundary_T4(:,2) > 120;
% mask = logical(mask .* (xy_coordinates_boundary_T4(:,1) < 400));
% mask = logical(mask .* (xy_coordinates_boundary_T4(:,1) > 100));
   
% SUN1_p18:
mask = xy_coordinates_boundary_T4(:,2) > 150;
mask = logical(mask .* (xy_coordinates_boundary_T4(:,2) < 400));


p = fit(xy_coordinates_boundary_T4(mask,1),xy_coordinates_boundary_T4(mask,2), 'poly2');

xrange=[0:0.5:max(resolutions(:,1))];

for i=1:size(xrange,2)
    
    yrange(i)=((xrange(i)^2)*p(1))+((xrange(i))*p(2))+p(3);
    
end

figure(1);
clf
hold on
plot(xy_coordinates_boundary_T4(:,1),xy_coordinates_boundary_T4(:,2),'ob');
plot(xy_coordinates_rest_T4(:,1),xy_coordinates_rest_T4(:,2),'or');
plot(xy_coordinates_boundary_T4(mask,1),xy_coordinates_boundary_T4(mask,2),'xg');
plot(p);
set(gca,'LineWidth',2.5,'FontSize',14);
xlim([0 max(resolutions(:,1))]);
ylim([0 max(resolutions(:,2))]);
xlabel('x pixel');
ylabel('y pixel');
plot(xrange,yrange,'--k','LineWidth',2);

print(gcf, '-dsvg', save_path + "boundary_fit.svg");
disp("Done with section 7")

%% Section 8 - CLASSIFY L1 NUCLEI ACCORDING WITH REGION

% not L1 = 0
% L1 boundary = 1
% L1 meristem = 2
% L1 organ = 3
selectedNuclei_boundary=L1_nuclei_boundary_selected_T4;
L1_nucleus_region_T4=zeros(1,size(nuclearData,2));
% count=0;

for i=1:size(L1_nuclei{4},1)
    
    %i
    
    if ismember(L1_nuclei{4}(i),selectedNuclei_boundary)==1
        
        L1_nucleus_region_T4(L1_nuclei{4}(i))=1;
        %count=count+1
        
    else
        
        centroid = nuclearData{4,L1_nuclei{4}(i)}.Centroid;
%         region_check=(centroid(1)*p.p1)+p.p2;
        region_check=((centroid(1)^2)*p.p1)+((centroid(1))*p.p2)+p.p3;
        
        
        if centroid(2)<region_check %%% Flip to swap organ-meristem.
            
            L1_nucleus_region_T4(L1_nuclei{4}(i))=2;
            %count=count+1
            
        else
            
            L1_nucleus_region_T4(L1_nuclei{4}(i))=3;
            %count=count+1
            
        end

    end
    

end
disp("Done with section 8")

%% Section 9 - Plot 2D map of selected nuclei IDs
% This is pretty slow, expect it to take more than 5 minutes.
% It is also pretty unnecessary to repeat, skip if you can.
if false
selectedNuclei_boundary=L1_nuclei_boundary_selected_T4;

color_boundary=rgb('Green');
color_meristem=rgb('Red');
color_organ=rgb('DarkViolet');
color_background=rgb('Azure');

%time=[24:12:32];%time vector (change sampling and end point)

% for i=1%Loop through each time point. NOTE: i is not used!
    
    %i %counter to follow progress (time point)
    i = 1;
    h=figure(i);
    clf
    hold on
    
    for m=1:size(L1_nuclei{4},1)
        disp( m + " of " + size(L1_nuclei{4},1))

        a=L1_nuclei{4}(m);
             
        for k=1:size(nuclearData{4,a}.PixelList,1)%Plot each pixel inside a given region with the appropriate color
                
            if L1_nucleus_region_T4(a)==1
                
                plot(nuclearData{4,a}.PixelList(k,1),nuclearData{4,a}.PixelList(k,2),'.','Color',color_boundary,'LineWidth',0.01);
                check=1;
                
            elseif L1_nucleus_region_T4(a)==2

                plot(nuclearData{4,a}.PixelList(k,1),nuclearData{4,a}.PixelList(k,2),'.','Color',color_meristem,'LineWidth',0.01);
                check=2;
                
            elseif L1_nucleus_region_T4(a)==3

                
                plot(nuclearData{4,a}.PixelList(k,1),nuclearData{4,a}.PixelList(k,2),'.','Color',color_organ,'LineWidth',0.01);
                check=3;
                
            end
            
            
        end
        
    end
    
    % Plot the black borders around the nuclei.
    for j=1:size(L1_nuclei{4},1)%Loop through each nucleus in a given time point
            
        j %counter to follow progress (nucleus number)
            
        a=L1_nuclei{4}(j);

        if isempty(nuclearData{4,a})==0 %if element is not empty

            % Get centroid and orientation and calculate X and Y major
            % and minor axes

            centroid = nuclearData{4,a}.Centroid;
            orientation = nuclearData{4,a}.Orientation;
            xMajor=centroid(1) + [-1 1]*(nuclearData{4,a}.MajorAxisLength/2)*cosd(orientation);
            yMajor=centroid(2) - [-1 1]*(nuclearData{4,a}.MajorAxisLength/2)*sind(orientation);
            xMinor=centroid(1) - [-1 1]*(nuclearData{4,a}.MinorAxisLength/2)*sind(orientation);
            yMinor=centroid(2) - [-1 1]*(nuclearData{4,a}.MinorAxisLength/2)*cosd(orientation);
                
            %plot convex hull defining outter boundary of segmented nucleus
                
            plot(nuclearData{4,a}.ConvexHull(:,1),nuclearData{4,a}.ConvexHull(:,2),'-k','LineWidth',2);
  
        end
        
    end
    
    %set plot formatting variables
    set(gca,'LineWidth',2.5,'FontSize',14);
    xlim([0 max(resolutions(:,1))]);
    ylim([0 max(resolutions(:,2))]);
    xlabel('x pixel');
    ylabel('y pixel');
	%print figure into PDF file (change name as needed)
% 	fname = sprintf('CRWN4_pl11_T4_regions.pdf');%,time(i));
% 	print(h, '-dpdf', fname)
%        
disp("starting to save plot")
tic()
print(h, '-dsvg', save_path + "2D_region_map_t" + 4 + ".svg");
toc()
% end
disp("Done with section 9")
else
    disp("Section 9 was disabled (if false), skipping.")
end

%% Section 10 - Plot 2D map of selected nuclei IDs
% Don't plot each pixel, but rather fill the convex hulls.

selectedNuclei_boundary=L1_nuclei_boundary_selected_T4;

color_boundary=rgb('Green');
color_meristem=rgb('Red');
color_organ=rgb('DarkViolet');
color_background=rgb('Azure');
color_list = {color_boundary, color_meristem, color_organ};

%time=[24:12:32];%time vector (change sampling and end point)

% for i=1%Loop through each time point. NOTE: i is not used!
    
    %i %counter to follow progress (time point)
    i = 1;
    h=figure(i);
    clf
    hold on
    

    
    % Plot the black borders around the nuclei.
    for j=1:size(L1_nuclei{4},1)%Loop through each nucleus in a given time point
            
%         j %counter to follow progress (nucleus number)
            
        a=L1_nuclei{4}(j);

        if isempty(nuclearData{4,a})==0 %if element is not empty

            % Get centroid and orientation and calculate X and Y major
            % and minor axes

            centroid = nuclearData{4,a}.Centroid;
            orientation = nuclearData{4,a}.Orientation;
            xMajor=centroid(1) + [-1 1]*(nuclearData{4,a}.MajorAxisLength/2)*cosd(orientation);
            yMajor=centroid(2) - [-1 1]*(nuclearData{4,a}.MajorAxisLength/2)*sind(orientation);
            xMinor=centroid(1) - [-1 1]*(nuclearData{4,a}.MinorAxisLength/2)*sind(orientation);
            yMinor=centroid(2) - [-1 1]*(nuclearData{4,a}.MinorAxisLength/2)*cosd(orientation);
                
            %plot convex hull defining outter boundary of segmented nucleus
                
            fill(nuclearData{4,a}.ConvexHull(:,1),nuclearData{4,a}.ConvexHull(:,2),color_list{L1_nucleus_region_T4(a)});
            plot(nuclearData{4,a}.ConvexHull(:,1),nuclearData{4,a}.ConvexHull(:,2),'-k','LineWidth',2);

        end
        
    end
    

    
    %set plot formatting variables
    set(gca,'LineWidth',2.5,'FontSize',14);
    xlim([0 max(resolutions(:,1))]);
    ylim([0 max(resolutions(:,2))]);
    xlabel('x pixel');
    ylabel('y pixel');
	%print figure into PDF file (change name as needed)
% 	fname = sprintf('CRWN4_pl11_T4_regions.pdf');%,time(i));
% 	print(h, '-dpdf', fname)
%        
disp("starting to save plot")
tic()
print(h, '-dsvg', save_path + "2D_region_map_filled_hulls_t" + 4 + ".svg");
toc()
% end
disp("Done with section 10")

%% Section 11 - GET ALL LINEAGES FOR THE THREE DIFFERENT REGIONS
% Method 1: the region of a given cell is defined at the last time point
% and then backtracked for the whole lineage
% L1 boundary = 1
% L1 meristem = 2
% L1 organ = 3

meristem_nuclei_T4=[];
organ_nuclei_T4=[];
boundary_nuclei_T4=[];

k=1;
l=1;
m=1;

disp("loop 1")
for i=1:size(L1_nucleus_region_T4,2)
    if L1_nucleus_region_T4(i)==1
        
        boundary_nuclei_T4(k)=i;
        k=k+1;
        
    elseif L1_nucleus_region_T4(i)==2
        
        meristem_nuclei_T4(l)=i;
        l=l+1;
        
    elseif L1_nucleus_region_T4(i)==3
        
        organ_nuclei_T4(m)=i;
        m=m+1;
        
    end
    
end


% Get lineages for BOUNDARY

selectedLineages_boundary=[];
selectedLineages_boundary_area_corrected=[];
selectedLineages_boundary_eccentricity=[];
selectedLineages_boundary_aspectRatio=[];
count=1;

% L1_nuclei_boundary_T4 are manually selected nuclei located at the
% boundary at the last time point
disp("loop 2")
for i=1:size(boundary_nuclei_T4,2)
    a=find(allLineageTrace(:,4)==boundary_nuclei_T4(i));
    
    if isempty(a)~=1
        
        if all(arrayfun(@(t)[~isTouchingBorder(nuclearData{t, allLineageTrace(a,t)}, resolutions(t,:))], 1:4))
            selectedLineages_boundary(count,:)=allLineageTrace(a,:);
            selectedLineages_boundary_area_corrected(count,:)=lineage_area_corrected(a,:);
            selectedLineages_boundary_eccentricity(count,:)=lineage_eccentricity(a,:);
            selectedLineages_boundary_aspectRatio(count,:)=lineage_aspect_ratio(a,:);
            count=count+1;
        end
    end
    
end

% Get lineages for MERISTEM

selectedLineages_meristem=[];
selectedLineages_meristem_area_corrected=[];
selectedLineages_meristem_eccentricity=[];
selectedLineages_meristem_aspectRatio=[];
count=1;

% L1_nuclei_meristem_T4 are manually selected nuclei located at the
% meristem at the last time point
disp("loop 3")
for i=1:size(meristem_nuclei_T4,2)
    a=find(allLineageTrace(:,4)==meristem_nuclei_T4(i));
    
    if isempty(a)~=1
        % The following if statment filters out lineages where one cell is
        % cropped at the boundary of the image.
        if all(arrayfun(@(t)[~isTouchingBorder(nuclearData{t, allLineageTrace(a,t)}, resolutions(t,:))], 1:4))
            selectedLineages_meristem(count,:)=allLineageTrace(a,:);
            selectedLineages_meristem_area_corrected(count,:)=lineage_area_corrected(a,:);
            selectedLineages_meristem_eccentricity(count,:)=lineage_eccentricity(a,:);
            selectedLineages_meristem_aspectRatio(count,:)=lineage_aspect_ratio(a,:);
            count=count+1;
        end
        
    end
    
end

% Get lineages for ORGAN

selectedLineages_organ=[];
selectedLineages_organ_area_corrected=[];
selectedLineages_organ_eccentricity=[];
selectedLineages_organ_aspectRatio=[];
count=1;

% L1_nuclei_organ_T4 are manually selected nuclei located at the
% organ at the last time point
disp("loop 4")
for i=1:size(organ_nuclei_T4,2)
    a=find(allLineageTrace(:,4)==organ_nuclei_T4(i));
    
    if isempty(a)~=1
        if all(arrayfun(@(t)[~isTouchingBorder(nuclearData{t, allLineageTrace(a,t)}, resolutions(t,:))], 1:4))
            selectedLineages_organ(count,:)=allLineageTrace(a,:);
            selectedLineages_organ_area_corrected(count,:)=lineage_area_corrected(a,:);
            selectedLineages_organ_eccentricity(count,:)=lineage_eccentricity(a,:);
            selectedLineages_organ_aspectRatio(count,:)=lineage_aspect_ratio(a,:);
            count=count+1;
        end
        
    end
    
end
disp("Done with section 11")

%% Section 12 - GET CURVATURE VALUES FOR EACH LINEAGE IN EACH REGION


% Get the curvature values for all the lineages in BOUNDARY
selectedLineages_boundary_curvature_invert=[];
selectedLineages_boundary_invert=selectedLineages_boundary';

for i=1:size(selectedLineages_boundary_invert,1)
    
%     i
   
    for j=1:size(selectedLineages_boundary_invert,2)
        
        selectedLineages_boundary_curvature_invert(i,j)=cell_curvatures(i,selectedLineages_boundary_invert(i,j));
       
    end
   
end

selectedLineages_boundary_curvature=selectedLineages_boundary_curvature_invert';

% Get the curvature values for all the lineages in MERISTEM
selectedLineages_meristem_curvature_invert=[];
selectedLineages_meristem_invert=selectedLineages_meristem';

for i=1:size(selectedLineages_meristem_invert,1)
    
%     i
   
    for j=1:size(selectedLineages_meristem_invert,2)
        
        selectedLineages_meristem_curvature_invert(i,j)=cell_curvatures(i,selectedLineages_meristem_invert(i,j));
       
    end
   
end

selectedLineages_meristem_curvature=selectedLineages_meristem_curvature_invert';

% Get the curvature values for all the lineages in organ
selectedLineages_organ_curvature_invert=[];
selectedLineages_organ_invert=selectedLineages_organ';

for i=1:size(selectedLineages_organ_invert,1)
    
%     i
   
    for j=1:size(selectedLineages_organ_invert,2)
        
        selectedLineages_organ_curvature_invert(i,j)=cell_curvatures(i,selectedLineages_organ_invert(i,j));
       
    end
   
end

selectedLineages_organ_curvature=selectedLineages_organ_curvature_invert';

disp("Done with section 12")

%% Section 13 - Collect data for - HEATMAP OF FOLD CHANGES FOR A GIVEN VARIABLE FOR SELECTED LINEAGES IN ALL REGIONS AND TIME POINTS


allSelectedLineages=(cat(1,selectedLineages_boundary,selectedLineages_meristem,selectedLineages_organ))';
allSelectedLineages_area_corrected=(cat(1,selectedLineages_boundary_area_corrected,selectedLineages_meristem_area_corrected,selectedLineages_organ_area_corrected))';
allSelectedLineages_curvature=(cat(1,selectedLineages_boundary_curvature,selectedLineages_meristem_curvature,selectedLineages_organ_curvature))';
allSelectedLineages_aspectRatio=(cat(1,selectedLineages_boundary_aspectRatio,selectedLineages_meristem_aspectRatio,selectedLineages_organ_aspectRatio))';
allSelectedLineages_eccentricity=(cat(1,selectedLineages_boundary_eccentricity,selectedLineages_meristem_eccentricity,selectedLineages_organ_eccentricity))';
%allSelectedLineages_cellArea=(cat(1,selectedLineages_boundary_cellArea,selectedLineages_meristem_cellArea,selectedLineages_organ_cellArea))';
%allSelectedLineages_nucleusToCellArea=(cat(1,selectedLineages_boundary_nucleusToCellArea,selectedLineages_meristem_nucleusToCellArea,selectedLineages_organ_nucleusToCellArea))';

selectedLineages_boundary_area_corrected_foldChange = getFoldChange(selectedLineages_boundary_area_corrected);
selectedLineages_meristem_area_corrected_foldChange = getFoldChange(selectedLineages_meristem_area_corrected);
selectedLineages_organ_area_corrected_foldChange = getFoldChange(selectedLineages_organ_area_corrected);

selectedLineages_boundary_aspectRatio_foldChange = getFoldChange(selectedLineages_boundary_aspectRatio);
selectedLineages_meristem_aspectRatio_foldChange = getFoldChange(selectedLineages_meristem_aspectRatio);
selectedLineages_organ_aspectRatio_foldChange = getFoldChange(selectedLineages_organ_aspectRatio);

selectedLineages_boundary_curvature_foldChange = getFoldChange(selectedLineages_boundary_curvature);
selectedLineages_meristem_curvature_foldChange = getFoldChange(selectedLineages_meristem_curvature);
selectedLineages_organ_curvature_foldChange = getFoldChange(selectedLineages_organ_curvature);

allSelectedLineages_area_corrected_foldChange=(cat(1,selectedLineages_boundary_area_corrected_foldChange,selectedLineages_meristem_area_corrected_foldChange,selectedLineages_organ_area_corrected_foldChange))';
allSelectedLineages_aspectRatio_foldChange=(cat(1,selectedLineages_boundary_aspectRatio_foldChange,selectedLineages_meristem_aspectRatio_foldChange,selectedLineages_organ_aspectRatio_foldChange))';
allSelectedLineages_curvature_foldChange=(cat(1,selectedLineages_boundary_curvature_foldChange,selectedLineages_meristem_curvature_foldChange,selectedLineages_organ_curvature_foldChange))';

disp("Done with section 13")
%% Section 14 - PLOTSPREAD OF VARIABLES FOR EACH REGION

nr_highlights = 10;
[dummy, highlighted_boundary_lineages] = maxk(selectedLineages_boundary_aspectRatio(:,4), nr_highlights);
% Boundary
fig = plotSwarm(selectedLineages_boundary_area_corrected, highlighted_boundary_lineages, 'Area', 'Boundary');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_boundary_area.svg");

fig = plotSwarm(selectedLineages_boundary_curvature, highlighted_boundary_lineages, 'Curvature', 'Boundary');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_boundary_curvature.svg");

fig = plotSwarm(selectedLineages_boundary_aspectRatio, highlighted_boundary_lineages, 'Aspect ratio', 'Boundary');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_boundary_aspect_ratio.svg");

fig = plotSwarm(selectedLineages_boundary_eccentricity, highlighted_boundary_lineages, 'Eccentricity', 'Boundary');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_boundary_eccentricity.svg");

disp("Done with section 14")
%% Section 15 - Plot 2d heatmap on nuclear shapes
% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5)
% Aspect ratios
calculated_quantity = "inverted_inferno_aspect_ratio"

[fig1, fig2, fig3, fig4] = plotNucleiHeatmap(allSelectedLineages_aspectRatio, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_boundary_lineages);
% [fig1, fig2, fig3, fig4] = plotNucleiHeatmap(selectedLineages_boundary_aspectRatio, selectedLineages_boundary, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_boundary_lineages);
print(fig1, '-dsvg', save_path + "heatmap_highlighted_" + calculated_quantity + "_t0" + ".svg")
print(fig2, '-dsvg', save_path + "heatmap_highlighted_" + calculated_quantity + "_t12" + ".svg")
print(fig3, '-dsvg', save_path + "heatmap_highlighted_" + calculated_quantity + "_t24" + ".svg")
print(fig4, '-dsvg', save_path + "heatmap_highlighted_" + calculated_quantity + "_t36" + ".svg")

print(fig1, '-dpdf', save_path + "heatmap_highlighted_" + calculated_quantity + "_t0" + ".pdf")
print(fig2, '-dpdf', save_path + "heatmap_highlighted_" + calculated_quantity + "_t12" + ".pdf")
print(fig3, '-dpdf', save_path + "heatmap_highlighted_" + calculated_quantity + "_t24" + ".pdf")
print(fig4, '-dpdf', save_path + "heatmap_highlighted_" + calculated_quantity + "_t36" + ".pdf")

disp("Done with section 15")
%% Section 16

highlighted_meristem_lineages = [];
% Meristem
fig = plotSwarm(selectedLineages_meristem_area_corrected, highlighted_meristem_lineages, 'Area', 'Meristem');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_meristem_area.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_meristem_area.pdf");

fig = plotSwarm(selectedLineages_meristem_curvature, highlighted_meristem_lineages, 'Curvature', 'Meristem');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_meristem_curvature.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_meristem_curvature.pdf");

fig = plotSwarm(selectedLineages_meristem_aspectRatio, highlighted_meristem_lineages, 'Aspect ratio', 'Meristem');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_meristem_aspect_ratio.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_meristem_aspect_ratio.pdf");

fig = plotSwarm(selectedLineages_meristem_eccentricity, highlighted_meristem_lineages, 'Eccentricity', 'Meristem');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_meristem_eccentricity.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_meristem_eccentricity.pdf");

disp("Done with section 16")

%% Section 17
highlighted_organ_lineages = [];
% Organ
fig = plotSwarm(selectedLineages_organ_area_corrected, highlighted_organ_lineages, 'Area', 'Organ');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_organ_area.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_organ_area.pdf");

fig = plotSwarm(selectedLineages_organ_curvature, highlighted_organ_lineages, 'Curvature', 'Organ');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_organ_curvature.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_organ_curvature.pdf");

fig = plotSwarm(selectedLineages_organ_aspectRatio, highlighted_organ_lineages, 'Aspect ratio', 'Organ');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_organ_aspect_ratio.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_organ_aspect_ratio.pdf");

fig = plotSwarm(selectedLineages_organ_eccentricity, highlighted_organ_lineages, 'Eccentricity', 'Organ');
print('-painters', '-dsvg', '-r600', save_path + "plotspread_organ_eccentricity.svg");
print('-painters', '-dpdf', '-r600', save_path + "plotspread_organ_eccentricity.pdf");

disp("Done with section 17")

%% Section 18 - Plot 2d heatmap on nuclear shapes
% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5)
highlighted_lineages = []
% Aspect ratios
calculated_quantity = "inverted_inferno_aspect_ratio"

[fig1, fig2, fig3, fig4] = plotNucleiHeatmap(allSelectedLineages_aspectRatio, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_lineages);
print(fig1, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t0" + ".svg")
print(fig2, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t12" + ".svg")
print(fig3, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t24" + ".svg")
print(fig4, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t36" + ".svg")

print(fig1, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t0" + ".pdf")
print(fig2, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t12" + ".pdf")
print(fig3, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t24" + ".pdf")
print(fig4, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t36" + ".pdf")

disp("Done with section 18")

%% Section 19

% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5)
highlighted_lineages = []
% Area
calculated_quantity = "inverted_inferno_area_corrected"
[fig1, fig2, fig3, fig4] = plotNucleiHeatmap(allSelectedLineages_area_corrected, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_lineages);
print(fig1, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t0" + ".svg")
print(fig2, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t12" + ".svg")
print(fig3, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t24" + ".svg")
print(fig4, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t36" + ".svg")

print(fig1, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t0" + ".pdf")
print(fig2, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t12" + ".pdf")
print(fig3, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t24" + ".pdf")
print(fig4, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t36" + ".pdf")

disp("Done with section 19")

%% Section 20

% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5)
highlighted_lineages = []
% Eccentricity
calculated_quantity = "inverted_inferno_eccentricity"
[fig1, fig2, fig3, fig4] = plotNucleiHeatmap(allSelectedLineages_eccentricity, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_lineages);
print(fig1, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t0" + ".svg")
print(fig2, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t12" + ".svg")
print(fig3, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t24" + ".svg")
print(fig4, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t36" + ".svg")

print(fig1, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t0" + ".pdf")
print(fig2, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t12" + ".pdf")
print(fig3, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t24" + ".pdf")
print(fig4, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t36" + ".pdf")

disp("Done with section 20")

%% Section 21

% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5)
highlighted_lineages = []
% Curvature
calculated_quantity = "inverted_inferno_curvature"
[fig1, fig2, fig3, fig4] = plotNucleiHeatmap(allSelectedLineages_curvature, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_lineages);
print(fig1, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t0" + ".svg")
print(fig2, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t12" + ".svg")
print(fig3, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t24" + ".svg")
print(fig4, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t36" + ".svg")

print(fig1, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t0" + ".pdf")
print(fig2, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t12" + ".pdf")
print(fig3, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t24" + ".pdf")
print(fig4, '-dpdf', save_path + "heatmap_" + calculated_quantity + "_t36" + ".pdf")



disp("Done with section 21")

%% Section 22 - PLOT CORRELATION BETWEEN TWO VARIABLES FOR DIFFERENT TISSUES
allSelectedLineages_vector_aspectRatio=cat(2,allSelectedLineages_aspectRatio(1,:),allSelectedLineages_aspectRatio(2,:),allSelectedLineages_aspectRatio(3,:),allSelectedLineages_aspectRatio(4,:));
allSelectedLineages_vector_curvature=cat(2,allSelectedLineages_curvature(1,:),allSelectedLineages_curvature(2,:),allSelectedLineages_curvature(3,:),allSelectedLineages_curvature(4,:));

selectedLineages_boundary_vector_aspectRatio=cat(2,selectedLineages_boundary_aspectRatio(:,1)',selectedLineages_boundary_aspectRatio(:,2)',selectedLineages_boundary_aspectRatio(:,3)',selectedLineages_boundary_aspectRatio(:,4)');
selectedLineages_meristem_vector_aspectRatio=cat(2,selectedLineages_meristem_aspectRatio(:,1)',selectedLineages_meristem_aspectRatio(:,2)',selectedLineages_meristem_aspectRatio(:,3)',selectedLineages_meristem_aspectRatio(:,4)');
selectedLineages_organ_vector_aspectRatio=cat(2,selectedLineages_organ_aspectRatio(:,1)',selectedLineages_organ_aspectRatio(:,2)',selectedLineages_organ_aspectRatio(:,3)',selectedLineages_organ_aspectRatio(:,4)');

selectedLineages_boundary_vector_curvature=cat(2,selectedLineages_boundary_curvature(:,1)',selectedLineages_boundary_curvature(:,2)',selectedLineages_boundary_curvature(:,3)',selectedLineages_boundary_curvature(:,4)');
selectedLineages_meristem_vector_curvature=cat(2,selectedLineages_meristem_curvature(:,1)',selectedLineages_meristem_curvature(:,2)',selectedLineages_meristem_curvature(:,3)',selectedLineages_meristem_curvature(:,4)');
selectedLineages_organ_vector_curvature=cat(2,selectedLineages_organ_curvature(:,1)',selectedLineages_organ_curvature(:,2)',selectedLineages_organ_curvature(:,3)',selectedLineages_organ_curvature(:,4)');

disp("Done with section 22")
%% Section 23

%
% ----------------------
% AREA
% ----------------------
color_boundary=rgb('DeepSkyBlue');
color_meristem=rgb('Red');
color_organ=rgb('DarkViolet');
color_background=rgb('Azure');

figure(1)
clf
hold on
plot(allSelectedLineages_vector_aspectRatio,allSelectedLineages_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_background,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
plot(selectedLineages_boundary_vector_aspectRatio,selectedLineages_boundary_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_boundary,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
l = lsline;
set(l,'LineWidth', 2)
set(gca,'LineWidth',2,'FontSize',16);
xlabel('Aspect Ratio')
ylabel('Curvature')

[RHO1,PVAL1]=corr(cat(2,selectedLineages_boundary_vector_aspectRatio',selectedLineages_boundary_vector_curvature'),'type','Spearman','Rows','complete')

% print -painters -dpdf -r600 CRWN4_pl5_aspectRatio_vs_curvature_boundary.pdf
print(gcf, '-dsvg', save_path + "aspect_ratio_vs_curvature_boundary.svg");

figure(2)
clf
hold on
plot(allSelectedLineages_vector_aspectRatio,allSelectedLineages_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_background,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
plot(selectedLineages_meristem_vector_aspectRatio,selectedLineages_meristem_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_meristem,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
l = lsline ;
set(l,'LineWidth', 2)
set(gca,'LineWidth',2,'FontSize',16);
xlabel('Aspect Ratio')
ylabel('Curvature')
xlim([0.5 2])

[RHO2,PVAL2]=corr(cat(2,selectedLineages_meristem_vector_aspectRatio',selectedLineages_meristem_vector_curvature'),'type','Spearman','Rows','complete')

% print -painters -dpdf -r600 CRWN4_pl5_aspectRatio_vs_curvature_meristem.pdf
print(gcf, '-dsvg', save_path + "aspect_ratio_vs_curvature_meristem.svg");

figure(3)
clf
hold on
plot(allSelectedLineages_vector_aspectRatio,allSelectedLineages_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_background,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
plot(selectedLineages_organ_vector_aspectRatio,selectedLineages_organ_vector_curvature,'o','MarkerEdgeColor',rgb('black'),'MarkerFaceColor',color_organ,'MarkerSize',6,'LineWidth',1,'Color',rgb('Black'))
l = lsline ;
set(l,'LineWidth', 2)
set(gca,'LineWidth',2,'FontSize',16);
xlabel('Aspect Ratio')
ylabel('Curvature')

[RHO3,PVAL3]=corr(cat(2,selectedLineages_organ_vector_aspectRatio',selectedLineages_organ_vector_curvature'),'type','Spearman','Rows','complete')

% print -painters -dpdf -r600 CRWN4_pl5_aspectRatio_vs_curvature_organ.pdf
print(gcf, '-dsvg', save_path + "aspect_ratio_vs_curvature_organ.svg");
disp("Done with section 23")


%% Section 24 - CORRELATIONS ASPECT RATIO vs CURVATURE (through time)

correlation_curvature_aspectRatio_boundary=[];

for i=1:size(selectedLineages_boundary,1)

    a=corr(cat(2,selectedLineages_boundary_curvature(i,:)',selectedLineages_boundary_aspectRatio(i,:)'),'Rows','complete');
    correlation_curvature_aspectRatio_boundary(i)=a(1,2);

end

correlation_curvature_aspectRatio_meristem=[];

for i=1:size(selectedLineages_meristem,1)

    a=corr(cat(2,selectedLineages_meristem_curvature(i,:)',selectedLineages_meristem_aspectRatio(i,:)'),'Rows','complete');
    correlation_curvature_aspectRatio_meristem(i)=a(1,2);

end

correlation_curvature_aspectRatio_organ=[];

for i=1:size(selectedLineages_organ,1)

    a=corr(cat(2,selectedLineages_organ_curvature(i,:)',selectedLineages_organ_aspectRatio(i,:)'),'Rows','complete');
    correlation_curvature_aspectRatio_organ(i)=a(1,2);

end

%Define classes, e.g. which rows correspond to a given reporter
boundary=correlation_curvature_aspectRatio_boundary;
meristem=correlation_curvature_aspectRatio_meristem;
organ=correlation_curvature_aspectRatio_organ;

%Make cell array with all classes
data={boundary,meristem,organ};

figure(1)
clf
plotSpread(data)
ylabel('Corr CellArea vs aspectRatio')%Change as suitable
xlim([0.5 3.5])%Change according to number of classes
ylim([-1 1])
set(gca,'XTickLabel',{'boundary','meristem','organ'},'LineWidth',2.5,'FontSize',16);
% NOTE: shape, size and color of the markers in the plot were changed
% manually by using the Figure Properties menu in the corresponding figure window
print(gcf, '-dsvg', save_path + "area_vs_aspect_ratio.svg");

disp("Done with section 24")

%% Section 25
% Use matlabs "regionprops" to extract features from the segmented data. This takes a few minutes.
membraneData={};
membrane_labels={};



tic()
for k=1:size(filelist_membrane_seg,1)

    fname=filelist_membrane_seg(k).name;%segmented filename

    % Get number of slices in both raw and segmented stacks
    a=strcat(filelist_membrane_seg(k).folder,"/",fname);%full path
    disp("Processing file " + a)
    info = imfinfo(a);
    num_images = numel(info);%nr of slices in segmented stack

    membrane_labels{k} = [];
    allFrames = cell(1,num_images);
    nr_cells = zeros(1,num_images);
    % Get region properties for all slices of the segmented stack
    for i=1:num_images % loops through each segmented slice

        disp("image " + k + ", i=" + i + " of " + num_images)
        img= imread(a,i);
        %for each slice get the properties of each segmented region
        %(corresponding to a nucleus)
        % Each element on 'allFrames' cell array contains the list of
        % properties for each segmented nucleus in that slice
        allFrames{1,i}=regionprops(img,'Area','BoundingBox','Centroid','ConvexHull','Eccentricity','MajorAxisLength','MinorAxisLength','Orientation','PixelList');
        nr_cells(1,i)=size(allFrames{1,i},1); %nr of nuclei segmented in each slice
        labels_in_slice = unique(img);
        membrane_labels{k} = union(membrane_labels{k}, labels_in_slice);
    end

    % Check that all the labels from morphographx also exists in the
    % segmented image.
    if length(union(membrane_labels{k}, label_correspondences{k}(:,2))) ~= length(membrane_labels{k})
        display(setdiff(union(membrane_labels{k}, label_correspondences{k}(:,2)), membrane_labels{k}))
        error("The Mgx correspondence uses a label which does not exist in the segmentation.")
    end

    total_cells(k)=max(nr_cells(1,:)); %total number of segmented nuclei over all slices
%     total_cells(k)=length(membrane_labels{k}); %total number of segmented nuclei over all slices

    % Loop through all slices for each nucleus, select largest slice and
    % calculate shape parameters
    for m=1%:size(allFrames{1,i},1)

        nr=[1:1:total_cells(k)];

        for j=1:size(nr,2)

            count_j(j)=j;

            allAreas=[];

            for i=1:size(allFrames(m,:),2)

                count_i(j,i)=i;

                if size(allFrames{m,i},1)>=nr(j)

                    allAreas(i)=allFrames{m,i}(nr(j)).Area;

                else
                    allAreas(i)=0;

                end

            end

            %allAreas_count(j,:)=allAreas;
            [maxVal,index]=max(allAreas);%maximum area annotation

            if(max(allAreas)==0)

                % Shape parameter annotation
                membraneData{k,j}.id=nr(j);
                membraneData{k,j}.PixelList=[0,0,0];
                membraneData{k,j}.Area=0;
                membraneData{k,j}.Eccentricity=0;
                membraneData{k,j}.AspectRatio=0;
                membraneData{k,j}.Area_corrected=0;


            else
                % Shape parameter annotation
                membraneData{k,j}=allFrames{m,index}(nr(j));
                membraneData{k,j}.id=nr(j);
                membraneData{k,j}.AspectRatio=membraneData{k,j}.MajorAxisLength / membraneData{k,j}.MinorAxisLength;
                membraneData{k,j}.PixelList(:,3)=index;
                membraneData{k,j}.AreaEccentricity=membraneData{k,j}.Area*membraneData{k,j}.Eccentricity;
                membraneData{k,j}.Area_corrected=membraneData{k,j}.Area*pixelSizes(1,k)*pixelSizes(2,k);

            end
        end
    end
end
clear allFrames nr_cells

disp("Done with section 25")
toc()

%% Section 26 - Check membrane - nucleus correspondence.
id = 384
timepoint = 4
ret_index = 0
for i = 1:size(membraneData,2)
    if isstruct(membraneData{timepoint, i})
        if membraneData{timepoint, i }.id == id
            ret_index = i
            break
        end

    end

end
membraneData{timepoint, ret_index}

disp("Done with section 26")

%% Section 27
nuclear_coord = []; % indices (x/y), (t_x), (i).
membrane_coord = []; % indices (x/y), (t_x), (i).
for coord = 1:2
    for timepoint = 1:4
        nuclear_coord(coord, timepoint, :) = arrayfun(@(i) nuclearData{timepoint,i}.Centroid(coord), allSelectedLineages(timepoint,:));
        membrane_coord(coord, timepoint, :) = arrayfun(@(i) membraneData{timepoint,i}.Centroid(coord), allSelectedLineages(timepoint,:));
    end
end


figure(1);
clf
hold on
scatter(nuclear_coord(1,4,:), nuclear_coord(2,4,:))
scatter(membrane_coord(1,4,:), membrane_coord(2,4,:))

%%% Test that the nuclear centroid is close to the membrane centroid.
% This is to ensure that the indices used actually point to the same cells
% in the two arrays. The plot should show a straight line.
figure(2)
clf
hold on
scatter(nuclear_coord(1,4,:), membrane_coord(1,4,:))
scatter(nuclear_coord(2,4,:), membrane_coord(2,4,:))


nuclear_area = []; % indices (x/y), (t_x), (i).
membrane_area = []; % indices (x/y), (t_x), (i).

for timepoint = 1:4
    nuclear_area(timepoint, :) = arrayfun(@(i) nuclearData{timepoint,i}.Area, allSelectedLineages(timepoint,:));
    membrane_area(timepoint, :) = arrayfun(@(i) membraneData{timepoint,i}.Area, allSelectedLineages(timepoint,:));
end

figure(3)
clf
hold on
scatter(membrane_area(4,:), nuclear_area(4,:), 5, rgb('grey'))
xlabel("Nuclear area")
ylabel("Cell area")

disp("Done with section 27")
%% Section 28
nuclear_area = zeros(size(nuclearData)); % indices (x/y), (t_x), (i).
membrane_area = zeros(size(membraneData)); % indices (x/y), (t_x), (i).

for timepoint = 1:size(nuclearData,1)
    for i = 1:size(nuclearData,2)
        if isstruct(nuclearData{timepoint, i})
            nuclear_area(timepoint, i) = nuclearData{timepoint,i}.Area_corrected;
        end

        if isstruct(membraneData{timepoint, i})
            membrane_area(timepoint, i) = membraneData{timepoint,i}.Area_corrected;
        end
    end
end
disp("Done with section 28")

%% Section 29 - Plot nuclear - membrane area correlation. 

figure(1)
clf
hold on

for t = 1:4
    scatter(membrane_area(t,allSelectedLineages(t,:)), nuclear_area(t,allSelectedLineages(t,:)), 20, [0.7, 0.7, 0.7])
end
for t = 1:4
    scatter(membrane_area(t,selectedLineages_boundary(:,t)), nuclear_area(t,selectedLineages_boundary(:,t)), 20, [0, 0.4470, 0.7410])
end



ylabel("Nuclear area [\mum^2]")
xlabel("Cell area [\mum^2]")
xlim([0 inf])
ylim([0 inf])

print(gcf, '-dsvg', save_path + "nuclear_vs_cell_area_boundary.svg");

%%%%
figure(2)
clf
hold on
for t = 1:4
    scatter(membrane_area(t,allSelectedLineages(t,:)), nuclear_area(t,allSelectedLineages(t,:)), 20, [0.7, 0.7, 0.7])
end

selected_nuclear_area = [];
selected_membrane_area = [];
for t = 1:4
    scatter(membrane_area(t,selectedLineages_meristem(:,t)), nuclear_area(t,selectedLineages_meristem(:,t)), 20, [0.8500, 0.3250, 0.0980])
selected_nuclear_area = cat(2, selected_nuclear_area, nuclear_area(t, selectedLineages_meristem(:,t)));
selected_membrane_area = cat(2, selected_membrane_area, membrane_area(t, selectedLineages_meristem(:,t)));
end
% f = fit( selected_membrane_area',selected_nuclear_area', 'poly1');
f2 = fitlm( selected_membrane_area',selected_nuclear_area', 1);
plot(f2)

legend('off')
ylabel("Nuclear area [\mum^2]")
xlabel("Cell area [\mum^2]")
xlim([0 inf])
ylim([0 inf])

print(gcf, '-dsvg', save_path + "nuclear_vs_cell_area_meristem.svg");

%%%%%
figure(1)
clf
hold on

for t = 1:4
    scatter(membrane_area(t,allSelectedLineages(t,:)), nuclear_area(t,allSelectedLineages(t,:)), 20, [0.7, 0.7, 0.7])
end
for t = 1:4
    scatter(membrane_area(t,selectedLineages_organ(:,t)), nuclear_area(t,selectedLineages_organ(:,t)), 20, [0.4940, 0.1840, 0.5560])
end

ylabel("Nuclear area [\mum^2]")
xlabel("Cell area [\mum^2]")
xlim([0 inf])
ylim([0 inf])

print(gcf, '-dsvg', save_path + "nuclear_vs_cell_area_organ.svg");

disp("Done with section 29")

